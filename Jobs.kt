package com.example.ticket.Upwork.Screen.Upwork

import android.util.Log
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight.Companion.Bold
import androidx.compose.ui.text.font.FontWeight.Companion.Light
import androidx.compose.ui.text.font.FontWeight.Companion.SemiBold
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Popup
import androidx.compose.ui.window.PopupProperties
import com.example.ticket.R
import com.example.ticket.Upwork.Data.Tag
import com.example.ticket.Upwork.Data.mockUserData
import com.example.ticket.Upwork.UpworkViewModel
import com.example.ticket.ui.theme.*
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
enum class Day() {
    Morning, Evening(), AllDay()
}
@Composable
fun Jobs() {
    var tag by remember{ mutableStateOf(Tag.All)}
    val viewModel = UpworkViewModel()
    var isExpanded by remember{mutableStateOf(false)}
    val simpleDateFormat = SimpleDateFormat("E dd MMM")
    val longerDateFormat = SimpleDateFormat("yyyy-MM-dd")
    val listState = rememberLazyListState()
    val coroutineScope = rememberCoroutineScope()
    var screenDate by remember { mutableStateOf(viewModel.todayString)}
    var partOfDay by remember{mutableStateOf(Day.AllDay)}
    var itemIndex by remember {mutableStateOf(0)}
    viewModel.sortedMockUserData.value = viewModel.sortedMockUserData.value.filter{simpleDateFormat.format(longerDateFormat.parse(it.date)) == screenDate}
    Scaffold(
        modifier = Modifier
            .background(Color.LightGray)
            .fillMaxSize(),
        topBar = {
            TopAppBar(modifier = Modifier
                .background(Color.White)
                .wrapContentHeight()
                .fillMaxWidth(),
            backgroundColor = Color.White) {
                Column(modifier = Modifier
                    .height(500.dp)
                    .fillMaxWidth()
                    .background(Color.White)) {
                    Row(modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.White)) {
                        Image(ImageVector.vectorResource(id = R.drawable.ic_baseline_keyboard_arrow_left_24),
                            "left",
                            contentScale = ContentScale.Crop,
                            modifier = Modifier
                                .size(35.dp)
                                .padding(start = 4.dp))
                    }
                    Row(modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.White)
                        .padding(horizontal = 3.dp),
                        horizontalArrangement = Arrangement.SpaceBetween) {
                        Row(modifier = Modifier.fillMaxWidth(0.42f)) {
                            IconButton(onClick = {screenDate = viewModel.todayString }) {
                                Image(ImageVector.vectorResource(id = R.drawable.ic_baseline_keyboard_arrow_left_24),
                                "Left", colorFilter = ColorFilter.tint(Color.Black))
                            }

                            Text(text = "$screenDate", color = Color.Black, fontWeight = Bold, modifier = Modifier.clickable { isExpanded =  !isExpanded })
                            IconButton(onClick= {screenDate = viewModel.tomorrowString}) {
                                Image(ImageVector.vectorResource(id = R.drawable.ic_baseline_keyboard_arrow_right_24),
                                    "Right")
                            }
                            }
                        Row(modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween) {
                            var morningClicked by remember{mutableStateOf(false)}
                            var eveningClicked by remember {mutableStateOf(false)}
                            var pmItem by remember{mutableStateOf(0)}
                            IconButton(onClick = {
                                coroutineScope.launch {
                                    viewModel.partOfDay.value = Day.Morning
                                    morningClicked = !morningClicked; eveningClicked = false
                                    listState.animateScrollToItem(0)
                                }}) {
                                Image(ImageVector.vectorResource(id = R.drawable.ic_baseline_wb_sunny_24),
                                    "Sunny",
                                    colorFilter = ColorFilter.tint(if (morningClicked) Color.Red else Color.Black))
                            }
                                Text(text = "Morning", color = if(!morningClicked) Color.Black else Color.Red)
                            IconButton(onClick = {coroutineScope.launch {
                                viewModel.partOfDay.value = Day.Evening ; eveningClicked = !eveningClicked; morningClicked = false
                                listState.animateScrollToItem(itemIndex)

                            }
                            }) {
                                Image(ImageVector.vectorResource(id = R.drawable.ic_baseline_nights_stay_24),
                                    "Night", colorFilter = ColorFilter.tint(if(eveningClicked) Color.Red else Color.Black))
                                }
                            Text(text = "Evening", color = if(!eveningClicked) Color.Black else Color.Red)
                        }

                    }
                }
            }

        }) {
        var onSelected by remember{mutableStateOf(false)}
        var buttonBackColor by remember{mutableStateOf(Color.White)}
        var buttonTextColor by remember{mutableStateOf(Color.Black)}
        var currentTime by mutableStateOf("00:00 AM")
        val bypass = 1
        Column(modifier = Modifier.fillMaxHeight()) {
            PopupWindowDialog(
                isExpanded, viewModel,
                onClickToday = {
                    isExpanded = !isExpanded
                    screenDate = viewModel.todayString
                },
                onClickTomorrow = {
                    isExpanded = !isExpanded
                    screenDate = viewModel.tomorrowString
                    Log.e("Jobs","Tomorrow")
                                  },
                onClickAfterTomorrow = {
                    isExpanded = !isExpanded
                    screenDate = viewModel.afterTomorrowString
                })
            LazyRow(modifier = Modifier
                .fillMaxWidth()
                .padding(top = 12.dp)) {
                items(items = Tag.values(), itemContent = { item ->
                    TextButton(
                        colors = ButtonDefaults.buttonColors(backgroundColor = if(tag == item) Color.DarkGray else Color.White),
                        onClick = {
                            onSelected != onSelected;
                                  tag = item},
                        modifier = Modifier
                            .clip(RoundedCornerShape(10.dp))
                            .padding(start = 8.dp, end = 8.dp, top = 8.dp, bottom = 8.dp)
                            .border(0.8.dp, Color.LightGray, shape = RoundedCornerShape(10.dp))
                            .background(color = if (onSelected) Color.Black else Color.White), shape = RoundedCornerShape(10.dp)) {
                        Text(item.name,
                            color = if(onSelected) Color.White else Color.Black,
                            fontWeight = Light,
                            modifier = Modifier.padding(0.dp))
                    }
                })
            }
            Divider()
            LazyColumn(
                contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp),
                state = listState,
                modifier = Modifier
                    .fillMaxSize()
                    .background(Whiteish)) {
                itemsIndexed(when(tag){
                    Tag.All -> viewModel.sortedMockUserData.value
                    Tag.Cardio -> viewModel.sortedMockUserData.value.filter{ it.tag == Tag.Cardio && simpleDateFormat.format(longerDateFormat.parse(it.date)) == screenDate}
                    Tag.Dance -> viewModel.sortedMockUserData.value.filter{it.tag == Tag.Dance}
                    Tag.Yoga -> viewModel.sortedMockUserData.value.filter{it.tag == Tag.Yoga}
                    Tag.Strength -> viewModel.sortedMockUserData.value.filter{it.tag == Tag.Strength}
                }, itemContent = { index, item ->
                    if(item.time == "04:00 PM") {
                        itemIndex = index
                    }
                    if(item.time != currentTime || index == 0) {
                        Row(modifier = Modifier.fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically) {
                            Text(text = item.time)
                            Divider(color = Color.LightGray,
                                thickness = 0.5.dp,
                                startIndent = 12.dp)
                        }
                        currentTime = item.time
                    }
                    Log.e("here",currentTime)
                    Row(Modifier
                        .height(80.dp)
                        .fillMaxWidth()
                        .border(0.8.dp, Color.LightGray, shape = RoundedCornerShape(5.dp))
                        .clip(RoundedCornerShape(5.dp)),
                        horizontalArrangement = Arrangement.SpaceBetween) {
                        Column(verticalArrangement = Arrangement.Top, modifier = Modifier
                            .fillMaxWidth(0.03f)
                            .fillMaxHeight()
                            .padding(top = 13.dp)) {
                            Box(modifier = Modifier
                                .width(10.dp)
                                .height(8.dp)
                                .background(color = (
                                        when (item.tag) {
                                            Tag.Cardio -> Blue
                                            Tag.Dance -> DeepBlue
                                            Tag.Strength -> DeepDishPurple
                                            Tag.Yoga -> Golden
                                            else -> {
                                                Color.White
                                            }
                                        })))
                        }
                        Column(modifier = Modifier
                            .fillMaxWidth(0.6f)
                            .fillMaxHeight()
                            .padding(top = 8.dp),verticalArrangement = Arrangement.Top) {
                            Text(text = item.title, fontWeight = SemiBold, maxLines = 2, )
                            Spacer(modifier = Modifier.height(5.dp))
                            Row(modifier = Modifier
                                .wrapContentWidth()
                                .padding(bottom = 8.dp)) {
                                Text(text = "${item.instructor} | ${item.skill}",
                                    fontWeight = Light)
                            }
                        }
                        Column(modifier = Modifier
                            .fillMaxHeight()
                            .padding(start = 16.dp, top = 8.dp), verticalArrangement = Arrangement.SpaceBetween, horizontalAlignment = Alignment.End) {
                                Text(text = "${item.duration} min", fontWeight = Light, modifier = Modifier.padding(end=5.dp))
                                Row(modifier = Modifier.wrapContentWidth()) {
                                    Image(ImageVector.vectorResource(id = R.drawable.ic_baseline_share_24),"Share",modifier = Modifier
                                        .align(Alignment.CenterVertically)
                                        .size(20.dp))
                                    Spacer(modifier = Modifier.width(5.dp))
                                    TextButton(onClick = {},modifier = Modifier
                                        .align(Alignment.Bottom)
                                        .wrapContentHeight(), shape = RoundedCornerShape(12.dp)) {
                                        Text(text = "Join", color = Color.Red,modifier = Modifier.align(Alignment.Bottom))
                                    }
                                }
                            }
                        }

                    })

                }
            }
        }
    }
@Composable
fun PopupWindowDialog(isExpanded : Boolean = false, viewModel : UpworkViewModel, onClickToday : ()->Unit, onClickTomorrow : ()->Unit,onClickAfterTomorrow : ()->Unit) {
    val openDialog = remember { mutableStateOf(false) }
    openDialog.value = isExpanded
    val buttonTitle = remember {
        mutableStateOf("Show Pop Up Window")
    }
        Box() {
            val popupWidth = 130.dp
            val popupHeight = 50.dp
            if (openDialog.value) {
                buttonTitle.value = "Hide Popup"
                Popup(
                    alignment = Alignment.TopEnd,
                    properties = PopupProperties()
                ) {
                    Box(
                        modifier = Modifier
                            .size(popupWidth, popupHeight)
                            .padding(top = 5.dp, start = 16.dp)
                            .background(Color.White, RoundedCornerShape(10.dp))
                            .border(0.5.dp, Color.LightGray, shape = RoundedCornerShape(10.dp))
                    ) {
                        Column(
                            modifier = Modifier
                                .fillMaxHeight()
                                .padding(3.dp), verticalArrangement = Arrangement.Center
                        ) {
                            Row(modifier = Modifier.fillMaxSize(), horizontalArrangement = Arrangement.SpaceAround) {
                                PopUpColumn(dayChar = "${viewModel.todayString[0]}",
                                    dayNum = "${viewModel.todayString.subSequence(6, 8)}",onClick = onClickToday)
                                PopUpColumn(dayChar = "${viewModel.tomorrowString[0]}",
                                    dayNum = "${viewModel.tomorrowString.subSequence(6, 8)}", onClick = onClickTomorrow)
                                PopUpColumn(dayChar = "${viewModel.afterTomorrowString[0]}",
                                    dayNum = "${viewModel.afterTomorrowString.subSequence(6, 8)}", onClick = onClickAfterTomorrow)
                            }
                        }
                    }
                        }
                        }
                    }
                }

@Composable
fun PopUpColumn(dayChar : String, dayNum : String, onClick : ()->Unit) {
    Column(modifier = Modifier
        .fillMaxHeight()
        .clickable(true, "CLick Ne", onClick = onClick)) {
        Text(text = dayChar, fontWeight = SemiBold, modifier = Modifier.padding(bottom = 3.dp))
        Text(text = dayNum, fontWeight = Light)
    }
}


@Preview
@Composable
fun preview() {
   Jobs()

}