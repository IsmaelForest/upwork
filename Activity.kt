package com.example.ticket.Upwork.Data

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
    data class Activity(
        val title: String = "",
        val instructor: String = "",
        val duration: Int = 0,
        val joined: Boolean = false,
        val skill: Skill = Skill.Beginner,
        val tag: Tag,
        val date: String = "",
        val time: String = ""
    )

    enum class Skill {
        Beginner, Intermediate, Advanced, Expert
    }

    enum class Tag {
        All, Yoga, Dance, Cardio, Strength
    }
    val dateholder = "2022"
    fun parser() {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")

        val date : Date = simpleDateFormat.parse("2022-02-07")
    }
    val mockUserData = listOf(
        Activity(
            "21 dyas Yoga Beginner: Day 17",
            "Naveen & Shwetha", 35, false, Skill.Beginner, Tag.Yoga, "2022-07-07", "04:00 PM"
        ),
        Activity(
            "Hatha Yoga: Strength & Endurance",
            "Anil",
            60,
            false,
            Skill.Intermediate,
            Tag.Yoga,
            "2022-07-07",
            "09:30 AM"
        ),
        Activity(
            title = "Walk Fitness","Tom Arun",35,false, Skill.Beginner, Tag.Cardio,"2022-07-07","09:30 AM"
        ),
        Activity(
            title="Cardio : Upper Body", "Radhul Huldrom", 30, false, Skill.Intermediate, Tag.Cardio, "2022-07-07","09:30 AM"
        ),
        Activity(
            title = "Dance Fitness","Nandini & Naveen", 30, false, Skill.Beginner,Tag.Dance,"2022-07-07","09:30 AM"
        ),
        Activity(
            title="Walk Fitness","Tom Arun",20,false, Skill.Beginner,Tag.Cardio,"2022-07-07", "10:00 AM"
        ),
        Activity(
            title = "Dance Fitness","Nandini & Naveen", 30, false, Skill.Beginner,Tag.Dance,"2022-07-07","10:00 AM"
        ),
        Activity(
            title="Strength & Conditioning: Arms, Chest and Back","Rahul Huidrom",50,false, Skill.Intermediate, Tag.Strength, "2022-07-08","05:00 AM"
        ),
        Activity(
            title="Walk Fitness","Nandini Shetty",20,false, Skill.Beginner,Tag.Cardio,"2022-07-08", "05:00 AM"
        ),
        Activity(
            title = "Strength Training: Legs & Abs with Dumbbells","Nitran Ponnappa",55,false,Skill.Intermediate,Tag.Strength,"2022-07-08","05:00 AM"
        ),
        Activity(
            title = "Dance Fitness","Tom Arun", 50, false, Skill.Intermediate,Tag.Dance,"2022-07-08","05:00 AM"
        ),
        Activity(
            title = "Dance Special: Ranveer Singh Special","Tom & Simran", 30, false, Skill.Beginner,Tag.Dance,"2022-07-08","05:00 AM"
        ),
        Activity(
            "21 dyas Yoga Beginner: Day 17",
            "Naveen & Shwetha", 35, false, Skill.Beginner, Tag.Yoga, "2022-07-08", "05:00 AM"
        ),
        Activity(
            "Hatha Yoga: Strength & Endurance",
            "Anil",
            60,
            false,
            Skill.Intermediate,
            Tag.Yoga,
            "2022-07-08",
            "05:00 AM"
        ),

    )



