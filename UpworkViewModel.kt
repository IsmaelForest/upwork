package com.example.ticket.Upwork

import android.util.Log
import androidx.compose.runtime.*
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.ticket.Data.HexaCode.HexaCodeApi
import com.example.ticket.Upwork.Data.Activity
import com.example.ticket.Upwork.Data.mockUserData
import com.example.ticket.Upwork.Screen.Upwork.Day
import com.example.ticket.viewmodels.ColorViewModel
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class UpworkViewModel: ViewModel() {
    var sortedMockUserData  = mutableStateOf(mockUserData)
    var todayString by mutableStateOf("")
    var tomorrowString by mutableStateOf("")
    var afterTomorrowString by mutableStateOf("")
    var partOfDay = mutableStateOf(Day.AllDay)

    init {
        getData()
    }
    private fun getData() {
        viewModelScope.launch() {
            try {
                val today = Calendar.getInstance();
                todayString = simpleDateFormat.format(today.time)

                val tomorrow = Calendar.getInstance();tomorrow.add(Calendar.DAY_OF_WEEK,1)
                tomorrowString = simpleDateFormat.format(tomorrow.time)

                val afterTomorrow = Calendar.getInstance();afterTomorrow.add(Calendar.DAY_OF_WEEK,2)
                afterTomorrowString = simpleDateFormat.format(afterTomorrow.time)

                sortedMockUserData.value = mockUserData.sortedBy{ simpleTimeFormat.parse(it.time.replace("AM","a.m.").replace("PM","p.m."))}

                Log.e("upwork View Model", tomorrowString)
            } catch (e: Exception) {
                Log.e("upwork View Model", e.toString())

            }
        }
    }
    companion object {
        val simpleTimeFormat = SimpleDateFormat("hh:mm a")
        val simpleDateFormat = SimpleDateFormat("E dd MMM")
        val singleCharDateFormat = SimpleDateFormat("E")
    }
}